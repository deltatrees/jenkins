# Jenkins Container

This repository contains Dockerfile for running a [Jenkins](http://jenkins-ci.org/) Server.

# Base Docker Image

* [java](https://registry.hub.docker.com/_/java/)

# Docker Tags

`deltatrees/jenkins` provides multiple tagged images:

* `latest` (default) alias to the `latest` jenkins release
* `1.598` 

## Installation

1. Install [Docker](https://www.docker.com/).
2. Download [automated build](https://registry.hub.docker.com/u/deltatrees/jenkins/) from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull deltatrees/jenkins`
3. Create a folder to mount and execute `chown 102 folder` to grant jenkins user the folder rights.

## Usage
```
docker run --name jenkins -p 8080:8080 -v /data/jenkins:/var/jenkins_home -d deltatrees/jenkins:latest
```

## Exposed port
* 8080
* 50000

## ENV variables and defaults
* JENKINS_HOME "/var/jenkins_home"
* JENKINS_VERSION "1.598"

## Volumes
Mountable volumes are:

* /var/jenkins_home
